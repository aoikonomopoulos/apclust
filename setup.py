from setuptools import setup

setup(name="apclust",
      version="0.1",
      description=("Cluster text lines by means of the affinity propagation "
                   "algorithm, using the edit distance as the metric."),
      url="https://gitlab.com/aoikonomopoulos/apclust",
      author="Angelos Oikonomopoulos, Vasili Revelas",
      author_email="c@quasinomial.net, vasili.revelas@gmail.com",
      license="GNU GPL v3+",
      packages=["apclust"],
      install_requires=[
          "scikit-learn[alldeps]",
          "editdistance",
          "python-Levenshtein"
      ],
      entry_points={
          "console_scripts": ["apclust=apclust.command_line:main"],
      },
      zip_safe=False)
