#!/usr/bin/env python

import argparse
import logging
import sys
import timeit

from apclust import output
from apclust.clustering import affinity_clusters
from apclust.similarity import calculate_distances


def main():
    desc = ("Cluster lines of text based on edit distance using affinity "
            "propagation")
    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument("-v", "--verbose", action="store_true",
                        help="produce verbose output")
    parser.add_argument("file", nargs='?', type=argparse.FileType('r'),
                        default=sys.stdin, help="input file")
    out_grp = parser.add_mutually_exclusive_group()
    out_grp.add_argument("--output-median", action="store_true",
                         help=("print Levenshtein median of lines in each "
                               "cluster"))
    # args.output_lines is an int, or None for no line limit
    # To make another output option the default, set default=False and then
    # check if this option was set with `if args.output_lines is not False`
    out_grp.add_argument("--output-lines", nargs="?", type=int, metavar="N",
                         const=None, default=None,
                         help=("print first N lines of each cluster or all "
                               "lines if N is omitted (default)"))
    args = parser.parse_args()
    if args.output_median:
        print_output = output.median
    else:
        print_output = output.lines(args.output_lines)

    log_level = logging.WARNING
    if args.verbose:
        log_level = logging.INFO
    logging.basicConfig(level=log_level, format="%(message)s")

    lines = args.file.readlines()
    s = timeit.default_timer()
    distances = calculate_distances(lines)
    clusters = affinity_clusters(lines, distances)
    print_output(lines, clusters)
    duration = timeit.default_timer() - s
    logging.info("Execution took %.2fs", duration)
