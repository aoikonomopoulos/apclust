import logging
import timeit

from sklearn.cluster import affinity_propagation


def affinity_clusters(lines, distances):
    # XXX: Perhaps dedup first and assign the 'preference'
    # based on the number of repetitions?
    s = timeit.default_timer()
    _, labels = affinity_propagation(distances)
    duration = timeit.default_timer() - s
    logging.info("Affinity propagation took %.2fs", duration)
    clusters = {}
    for i, lab in enumerate(labels):
        if lab not in clusters:
            clusters[lab] = []
        clusters[lab].append(i)
    return clusters
