import editdistance
import logging
import numpy
import timeit


def calculate_distances(lines):
    s = timeit.default_timer()
    n = len(lines)
    distances = numpy.zeros([n, n])
    # The similarity matrix is symmetric across the diagonal; only
    # calculate the lower triangular elements and copy the values to
    # their mirrors on the upper side. Diagonal elements remain at 0.
    for i in range(1, n):
        word_x = lines[i]
        for j in range(0, i):
            word_y = lines[j]
            distances[i, j] = distances[j, i] = \
                -editdistance.eval(word_x, word_y)

    duration = timeit.default_timer() - s
    logging.info("Similarity matrix took %.2fs", duration)

    return distances
