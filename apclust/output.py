import sys

import Levenshtein


cluster_header = "cluster %d [%d lines]:"
indent = "\t"


def median(lines, clusters):
    for label, cluster in clusters.items():
        median = indent + Levenshtein.median([lines[i] for i in cluster])
        print(cluster_header % (label, len(cluster)))
        print(median)


def lines(limit=None):
    def printer(lines, clusters):
        for label, cluster in clusters.items():
            print(cluster_header % (label, len(cluster)))
            for i, item in enumerate(cluster):
                if i == limit:
                    print(indent + "[...]")
                    break
                sys.stdout.write(indent + lines[item])
    return printer
